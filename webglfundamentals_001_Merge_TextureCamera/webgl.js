"use strict";

function webgl() {};

function main() {
    // Get A WebGL context
    /** @type {HTMLCanvasElement} */
    var canvas = document.getElementById("canvas");
    var gl = canvas.getContext("webgl");
    if (!gl) {
        return;
    }


    var createFlattenedVertices = function(gl, vertices) {
        return webglUtils.createBufferInfoFromArrays(
            gl,
            primitives.makeRandomVertexColors(
                primitives.deindexVertices(vertices), {
                    vertsPerColor: 6,
                    rand: function(ndx, channel) {
                        return channel < 3 ? ((128 + Math.random() * 128) | 0) : 255;
                    }
                })
        );
    };

    // Create shape
    var programInfo = webglUtils.createProgramInfo(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
    var sphereBufferInfo_soleil = createFlattenedVertices(gl, primitives.createSphereVertices(20, 24, 60));
    var sphereBufferInfo_planet1 = createFlattenedVertices(gl, primitives.createSphereVertices(10, 12, 60));
    var sphereBufferInfo_planet2 = createFlattenedVertices(gl, primitives.createSphereVertices(10, 12, 60));
    var sphereBufferInfo_planet3 = createFlattenedVertices(gl, primitives.createSphereVertices(16, 17, 60));
    var sphereBufferInfo_planet4 = createFlattenedVertices(gl, primitives.createSphereVertices(10, 12, 60));
    var sphereBufferInfo_planet5 = createFlattenedVertices(gl, primitives.createSphereVertices(10, 12, 60));


 /* BASE TEXTURES */

 var program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
 // look up where the vertex data needs to go.
 var texcoordLocation = gl.getAttribLocation(program, "a_texcoords");
 // Create a buffer for texcoords.
 var buffer = gl.createBuffer();
 gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
 gl.enableVertexAttribArray(texcoordLocation);
 // We'll supply texcoords as floats.
 gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0);
 // Set Texcoords.
 setTexcoords(gl);

 /* ******************* */

 /* TEXTURE 1 */
 var texture_soleil = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_soleil);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image0 = new Image();

 image0.crossOrigin = "";
 image0.src = "resources/sun.jpg";
 image0.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_soleil);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image0);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 /* TEXTURE 2 */
 var texture_planet1 = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_planet1);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image1 = new Image();

 image1.crossOrigin = "";
 image1.src = "resources/jupiter.jpg";
 image1.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_planet1);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image1);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 /* TEXTURE 2 */
 var texture_planet2 = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_planet2);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image2 = new Image();

 image2.crossOrigin = "";
 image2.src = "resources/make.jpg";
 image2.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_planet2);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image2);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 /* TEXTURE 3 */
 var texture_planet3 = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_planet3);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image3 = new Image();

 image3.crossOrigin = "";
 image3.src = "resources/earth.jpg";
 image3.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_planet3);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image3);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 /* TEXTURE 4 */
 var texture_planet4 = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_planet4);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image4 = new Image();

 image4.crossOrigin = "";
 image4.src = "resources/mars.jpg";
 image4.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_planet4);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image4);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 /* TEXTURE 5 */
 var texture_planet5 = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_planet5);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image5 = new Image();

 image5.crossOrigin = "";
 image5.src = "resources/saturn.jpg";
 image5.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_planet5);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image5);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 /* TEXTURE 6 */
 var texture_planet6 = gl.createTexture();
 gl.bindTexture(gl.TEXTURE_2D, texture_planet6);
 // Fill the texture with a 1x1 blue pixel.
 gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
     new Uint8Array([0, 0, 255, 255]));
 // Asynchronously load an image
 var image6 = new Image();

 image6.crossOrigin = "";
 image6.src = "resources/venus.jpg";
 image6.addEventListener('load', function() {
     // Now that the image has loaded make copy it to the texture.
     gl.bindTexture(gl.TEXTURE_2D, texture_planet6);
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image6);
     gl.generateMipmap(gl.TEXTURE_2D);
 });

 // Fill the buffer with texture coordinates for a plane.
 function setTexcoords(gl) {
     gl.bufferData(
         gl.ARRAY_BUFFER,
         new Float32Array(
             [
                 -3, -1,
                 2, -1,
                 -3, 4,
                 -3, 4,
                 2, -1,
                 2, 4,
             ]),
         gl.STATIC_DRAW);
 }




 // Uniforms for each object.

 var sphereUniforms_soleil = {
    u_texture: texture_soleil,
    u_matrix: m4.identity(),
};
var sphereUniforms_planet1 = {
    u_texture: texture_planet1,
    u_matrix: m4.identity(),
};
var sphereUniforms_planet2 = {
    u_texture: texture_planet2,
    u_matrix: m4.identity(),
};
var sphereUniforms_planet3 = {
    u_texture: texture_planet3,
    u_matrix: m4.identity(),
};
var sphereUniforms_planet4 = {
    u_texture: texture_planet4,
    u_matrix: m4.identity(),
};
var sphereUniforms_planet5 = {
    u_texture: texture_planet5,
    u_matrix: m4.identity(),
};

var sphereTranslation_soleil = [-120, 0, 0];
var sphereTranslation_planet1 = [-40, 0, 0];
var sphereTranslation_planet2 = [0, 0, 0];
var sphereTranslation_planet3 = [40, 0, 0];
var sphereTranslation_planet4 = [80, 0, 0];
var sphereTranslation_planet5 = [120, 0, 0];

var objectsToDraw = [

    {
        programInfo: programInfo,
        bufferInfo: sphereBufferInfo_soleil,
        uniforms: sphereUniforms_soleil,
    },

    {
        programInfo: programInfo,
        bufferInfo: sphereBufferInfo_planet1,
        uniforms: sphereUniforms_planet1,
    },

    {
        programInfo: programInfo,
        bufferInfo: sphereBufferInfo_planet2,
        uniforms: sphereUniforms_planet2,
    },

    {
        programInfo: programInfo,
        bufferInfo: sphereBufferInfo_planet3,
        uniforms: sphereUniforms_planet3,
    },

    {
        programInfo: programInfo,
        bufferInfo: sphereBufferInfo_planet4,
        uniforms: sphereUniforms_planet4,
    },

    {
        programInfo: programInfo,
        bufferInfo: sphereBufferInfo_planet5,
        uniforms: sphereUniforms_planet5,
    },

];


    function computeMatrix(viewProjectionMatrix, translation, xRotation, yRotation) {
        var matrix = m4.translate(viewProjectionMatrix,
            translation[0],
            translation[1],
            translation[2]);
        matrix = m4.xRotate(matrix, xRotation);
        return m4.yRotate(matrix, yRotation);
    }

    

    // setup GLSL program
    var program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);

    // look up where the vertex data needs to go.
    var positionLocation = gl.getAttribLocation(program, "a_position");
    var colorLocation = gl.getAttribLocation(program, "a_color");

    // lookup uniforms
    var matrixLocation = gl.getUniformLocation(program, "u_matrix");

    // Create a buffer to put positions in
    var positionBuffer = gl.createBuffer();
    // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    function radToDeg(r) {
        return r * 180 / Math.PI;
    }

    function degToRad(d) {
        return d * Math.PI / 180;
    }

    // Initial parameters of the scene
    var translation = [-150, 0, -360];
    var rotation = [degToRad(190), degToRad(40), degToRad(320)];
    var scale = [1, 1, 1];
    var fieldOfViewRadians = degToRad(60);

    requestAnimationFrame(drawScene);

    // Setup a ui.
    webglLessonsUI.setupSlider("#x", {
        value: translation[0],
        slide: updatePosition(0),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#y", {
        value: translation[1],
        slide: updatePosition(1),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#z", {
        value: translation[2],
        slide: updatePosition(2),
        min: -1000,
        max: 0
    });
    webglLessonsUI.setupSlider("#angleX", {
        value: radToDeg(rotation[0]),
        slide: updateRotation(0),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleY", {
        value: radToDeg(rotation[1]),
        slide: updateRotation(1),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleZ", {
        value: radToDeg(rotation[2]),
        slide: updateRotation(2),
        max: 360
    });

    // Methode of ui
    function updatePosition(index) {
        return function(event, ui) {
            translation[index] = ui.value;
            requestAnimationFrame(drawScene);
        };
    }

    function updateRotation(index) {
        return function(event, ui) {
            var angleInDegrees = ui.value;
            var angleInRadians = angleInDegrees * Math.PI / 180;
            rotation[index] = angleInRadians;
            requestAnimationFrame(drawScene);
        };
    }


    // Draw the scene.
    function drawScene(time) {

        time *= 0.0005;

        webglUtils.resizeCanvasToDisplaySize(gl.canvas);

        // Tell WebGL how to convert from clip space to pixels
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear the canvas AND the depth buffer.
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        // Turn on culling. By default backfacing triangles
        // will be culled.
        gl.enable(gl.CULL_FACE);

        // Enable the depth buffer
        gl.enable(gl.DEPTH_TEST);

        // Tell it to use our program (pair of shaders)
        gl.useProgram(program);

        // Turn on the position attribute
        gl.enableVertexAttribArray(positionLocation);

        // Bind the position buffer.
        gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);


        // Compute the matrix
        var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        var zNear = 1;
        var zFar = 2000;
        var matrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
        matrix = m4.translate(matrix, translation[0], translation[1], translation[2]);
        matrix = m4.xRotate(matrix, rotation[0]);
        matrix = m4.yRotate(matrix, rotation[1]);
        matrix = m4.zRotate(matrix, rotation[2]);
        matrix = m4.scale(matrix, scale[0], scale[1], scale[2]);

        // Set the matrix.
        gl.uniformMatrix4fv(matrixLocation, false, matrix);

        var sphereXRotation_planet = time;
        var sphereYRotation_planet = time;
        var sphereXRotation_soleil = 0;
        var sphereYRotation_soleil = 0;

           // Compute the matrices for each object.
           sphereUniforms_soleil.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_soleil,
            sphereXRotation_soleil,
            sphereYRotation_soleil);

        sphereUniforms_planet1.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_planet1,
            sphereXRotation_planet,
            sphereYRotation_planet);

        sphereUniforms_planet2.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_planet2,
            sphereXRotation_planet,
            sphereYRotation_planet);

        sphereUniforms_planet3.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_planet3,
            sphereXRotation_planet,
            sphereYRotation_planet);

        sphereUniforms_planet4.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_planet4,
            sphereXRotation_planet,
            sphereYRotation_planet);

        sphereUniforms_planet5.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_planet5,
            sphereXRotation_planet,
            sphereYRotation_planet);

        // ------ Draw the objects --------

        objectsToDraw.forEach(function(object) {
            var programInfo = object.programInfo;
            var bufferInfo = object.bufferInfo;

            gl.useProgram(programInfo.program);

            // Setup all the needed attributes.
            webglUtils.setBuffersAndAttributes(gl, programInfo, bufferInfo);

            // Set the uniforms.
            webglUtils.setUniforms(programInfo, object.uniforms);

            // Draw
            gl.drawArrays(gl.TRIANGLES, 0, bufferInfo.numElements);
        });

        requestAnimationFrame(drawScene);
    }

    // Keyboad event
    window.addEventListener("keydown", function(event) {

        var n = 5;

        if (event.defaultPrevented) {
            return; // Ne devrait rien faire si l'événement de la touche était déjà consommé.
        }

        switch (event.key) {
            case "w":
                this.console.log("x--");
                webglLessonsUI.setupSlider("#x", {
                    value: translation[0] - n,
                    slide: updatePosition(0),
                    min: -200,
                    max: 200
                });
                translation[0] = translation[0] - n;
                break;
            case "x":
                this.console.log("x++");
                webglLessonsUI.setupSlider("#x", {
                    value: translation[0] + n,
                    slide: updatePosition(0),
                    min: -200,
                    max: 200
                });
                translation[0] = translation[0] + n;
                break;
            case "c":
                this.console.log("angleX--");
                webglLessonsUI.setupSlider("#angleX", {
                    value: (radToDeg(rotation[0]) - n),
                    slide: updateRotation(0),
                    max: 360
                });
                rotation[0] = (radToDeg(rotation[0]) - n) * Math.PI / 180;
                break;
            case "v":
                this.console.log("angleX++");
                webglLessonsUI.setupSlider("#angleX", {
                    value: (radToDeg(rotation[0]) + n),
                    slide: updateRotation(0),
                    max: 360
                });
                rotation[0] = (radToDeg(rotation[0]) + n) * Math.PI / 180;
                break;
            case "q":
                this.console.log("y--");
                webglLessonsUI.setupSlider("#y", {
                    value: translation[1] - n,
                    slide: updatePosition(1),
                    min: -200,
                    max: 200
                });
                translation[1] = translation[1] - n;
                break;
            case "s":
                this.console.log("y++");
                webglLessonsUI.setupSlider("#y", {
                    value: translation[1] + n,
                    slide: updatePosition(1),
                    min: -200,
                    max: 200
                });
                translation[1] = translation[1] + n;
                break;
            case "d":
                this.console.log("angleY--");
                webglLessonsUI.setupSlider("#angleY", {
                    value: (radToDeg(rotation[1]) - n),
                    slide: updateRotation(1),
                    max: 360
                });
                rotation[1] = (radToDeg(rotation[1]) - n) * Math.PI / 180;
                break;
            case "f":
                this.console.log("angleY++");
                webglLessonsUI.setupSlider("#angleY", {
                    value: (radToDeg(rotation[1]) + n),
                    slide: updateRotation(1),
                    max: 360
                });
                rotation[1] = (radToDeg(rotation[1]) + n) * Math.PI / 180;
                break;
            case "a":
                this.console.log("z--");
                webglLessonsUI.setupSlider("#z", {
                    value: translation[2] - n,
                    slide: updatePosition(2),
                    min: -1000,
                    max: 0
                });
                translation[2] = translation[2] - n;
                break;
            case "z":
                this.console.log("z++");
                webglLessonsUI.setupSlider("#z", {
                    value: translation[2] + n,
                    slide: updatePosition(2),
                    min: -1000,
                    max: 0
                });
                translation[2] = translation[2] + n;
                break;
            case "e":
                this.console.log("angleZ--");
                webglLessonsUI.setupSlider("#angleZ", {
                    value: (radToDeg(rotation[2]) - n),
                    slide: updateRotation(2),
                    max: 360
                });
                rotation[2] = (radToDeg(rotation[2]) - n) * Math.PI / 180;
                break;
            case "r":
                this.console.log("angleZ++");
                webglLessonsUI.setupSlider("#angleZ", {
                    value: (radToDeg(rotation[2]) + n),
                    slide: updateRotation(2),
                    max: 360
                });
                rotation[2] = (radToDeg(rotation[2]) + n) * Math.PI / 180;
                break;
            case "ArrowDown":
                console.log("ArrowDown");
                break;
            case "ArrowUp":
                this.console.log("ArrowUp");
                break;
            case "Enter":
                this.console.log("Enter");
                break;
            case " ":
                this.console.log("Space");
                break;
            default:
                return; // Quitter lorsque cela ne gère pas l'événement touche.
        }

        requestAnimationFrame(drawScene);

        // Annuler l'action par défaut pour éviter qu'elle ne soit traitée deux fois.
        event.preventDefault();
    }, true);

}

main();