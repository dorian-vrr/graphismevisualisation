"use strict";

function webgl() {};

function main() {
    // Get A WebGL context
    /** @type {HTMLCanvasElement} */
    var canvas = document.getElementById("canvas");
    var gl = canvas.getContext("webgl");
    if (!gl) {
        return;
    }


    var createFlattenedVertices = function(gl, vertices) {
        return webglUtils.createBufferInfoFromArrays(
            gl,
            primitives.makeRandomVertexColors(
                primitives.deindexVertices(vertices), {
                    vertsPerColor: 6,
                    rand: function(ndx, channel) {
                        return channel < 3 ? ((128 + Math.random() * 128) | 0) : 255;
                    }
                })
        );
    };

    // Create shape
    var programInfo = webglUtils.createProgramInfo(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
    var sphereBufferInfo_planet3 = createFlattenedVertices(gl, primitives.createSphereVertices(16, 17, 5));
    var cubeBufferInfo = createFlattenedVertices(gl, primitives.createCubeVertices(20));

    var sphereUniforms_planet3 = {
        u_colorMult: [0.5, 1, 0.5, 1],
        u_matrix: m4.identity(),
    };
    var cubeUniforms = {
        u_colorMult: [1, 0.5, 0.5, 1],
        u_matrix: m4.identity(),
    };
    var sphereTranslation_planet3 = [40, 0, 0];
    var cubeTranslation = [-40, 0, 0];

    var objectsToDraw = [

        {
            programInfo: programInfo,
            bufferInfo: sphereBufferInfo_planet3,
            uniforms: sphereUniforms_planet3,
        },
        {
            programInfo: programInfo,
            bufferInfo: cubeBufferInfo,
            uniforms: cubeUniforms,
        },

    ];

    function computeMatrix(viewProjectionMatrix, translation, xRotation, yRotation) {
        var matrix = m4.translate(viewProjectionMatrix,
            translation[0],
            translation[1],
            translation[2]);
        matrix = m4.xRotate(matrix, xRotation);
        return m4.yRotate(matrix, yRotation);
    }


    // setup GLSL program
    var program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);

    // look up where the vertex data needs to go.
    var positionLocation = gl.getAttribLocation(program, "a_position");
    var colorLocation = gl.getAttribLocation(program, "a_color");

    // lookup uniforms
    var matrixLocation = gl.getUniformLocation(program, "u_matrix");

    // Create a buffer to put positions in
    var positionBuffer = gl.createBuffer();
    // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    function radToDeg(r) {
        return r * 180 / Math.PI;
    }

    function degToRad(d) {
        return d * Math.PI / 180;
    }

    // Initial parameters of the scene
    var translation = [-150, 0, -360];
    var rotation = [degToRad(190), degToRad(40), degToRad(320)];
    var scale = [1, 1, 1];
    var fieldOfViewRadians = degToRad(60);

    drawScene();

    // Setup a ui.
    webglLessonsUI.setupSlider("#x", {
        value: translation[0],
        slide: updatePosition(0),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#y", {
        value: translation[1],
        slide: updatePosition(1),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#z", {
        value: translation[2],
        slide: updatePosition(2),
        min: -1000,
        max: 0
    });
    webglLessonsUI.setupSlider("#angleX", {
        value: radToDeg(rotation[0]),
        slide: updateRotation(0),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleY", {
        value: radToDeg(rotation[1]),
        slide: updateRotation(1),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleZ", {
        value: radToDeg(rotation[2]),
        slide: updateRotation(2),
        max: 360
    });

    // Methode of ui
    function updatePosition(index) {
        return function(event, ui) {
            translation[index] = ui.value;
            drawScene();
        };
    }

    function updateRotation(index) {
        return function(event, ui) {
            var angleInDegrees = ui.value;
            var angleInRadians = angleInDegrees * Math.PI / 180;
            rotation[index] = angleInRadians;
            drawScene();
        };
    }


    // Draw the scene.
    function drawScene() {

        webglUtils.resizeCanvasToDisplaySize(gl.canvas);

        // Tell WebGL how to convert from clip space to pixels
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear the canvas AND the depth buffer.
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        // Turn on culling. By default backfacing triangles
        // will be culled.
        gl.enable(gl.CULL_FACE);

        // Enable the depth buffer
        gl.enable(gl.DEPTH_TEST);

        // Tell it to use our program (pair of shaders)
        gl.useProgram(program);

        // Turn on the position attribute
        gl.enableVertexAttribArray(positionLocation);

        // Bind the position buffer.
        gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);


        // Compute the matrix
        var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        var zNear = 1;
        var zFar = 2000;
        var matrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
        matrix = m4.translate(matrix, translation[0], translation[1], translation[2]);
        matrix = m4.xRotate(matrix, rotation[0]);
        matrix = m4.yRotate(matrix, rotation[1]);
        matrix = m4.zRotate(matrix, rotation[2]);
        matrix = m4.scale(matrix, scale[0], scale[1], scale[2]);

        // Set the matrix.
        gl.uniformMatrix4fv(matrixLocation, false, matrix);

        var sphereXRotation_planet = 0;
        var sphereYRotation_planet = 0;

        sphereUniforms_planet3.u_matrix = computeMatrix(
            matrix,
            sphereTranslation_planet3,
            sphereXRotation_planet,
            sphereYRotation_planet);

        cubeUniforms.u_matrix = computeMatrix(
            matrix,
            cubeTranslation,
            sphereXRotation_planet,
            sphereYRotation_planet);

        // ------ Draw the objects --------

        objectsToDraw.forEach(function(object) {
            var programInfo = object.programInfo;
            var bufferInfo = object.bufferInfo;

            gl.useProgram(programInfo.program);

            // Setup all the needed attributes.
            webglUtils.setBuffersAndAttributes(gl, programInfo, bufferInfo);

            // Set the uniforms.
            webglUtils.setUniforms(programInfo, object.uniforms);

            // Draw
            gl.drawArrays(gl.TRIANGLES, 0, bufferInfo.numElements);
        });
    }

    // Keyboad event
    window.addEventListener("keydown", function(event) {

        var n = 5;

        if (event.defaultPrevented) {
            return; // Ne devrait rien faire si l'événement de la touche était déjà consommé.
        }

        switch (event.key) {
            case "w":
                this.console.log("x--");
                webglLessonsUI.setupSlider("#x", {
                    value: translation[0] - n,
                    slide: updatePosition(0),
                    min: -200,
                    max: 200
                });
                translation[0] = translation[0] - n;
                break;
            case "x":
                this.console.log("x++");
                webglLessonsUI.setupSlider("#x", {
                    value: translation[0] + n,
                    slide: updatePosition(0),
                    min: -200,
                    max: 200
                });
                translation[0] = translation[0] + n;
                break;
            case "c":
                this.console.log("angleX--");
                webglLessonsUI.setupSlider("#angleX", {
                    value: (radToDeg(rotation[0]) - n),
                    slide: updateRotation(0),
                    max: 360
                });
                rotation[0] = (radToDeg(rotation[0]) - n) * Math.PI / 180;
                break;
            case "v":
                this.console.log("angleX++");
                webglLessonsUI.setupSlider("#angleX", {
                    value: (radToDeg(rotation[0]) + n),
                    slide: updateRotation(0),
                    max: 360
                });
                rotation[0] = (radToDeg(rotation[0]) + n) * Math.PI / 180;
                break;
            case "q":
                this.console.log("y--");
                webglLessonsUI.setupSlider("#y", {
                    value: translation[1] - n,
                    slide: updatePosition(1),
                    min: -200,
                    max: 200
                });
                translation[1] = translation[1] - n;
                break;
            case "s":
                this.console.log("y++");
                webglLessonsUI.setupSlider("#y", {
                    value: translation[1] + n,
                    slide: updatePosition(1),
                    min: -200,
                    max: 200
                });
                translation[1] = translation[1] + n;
                break;
            case "d":
                this.console.log("angleY--");
                webglLessonsUI.setupSlider("#angleY", {
                    value: (radToDeg(rotation[1]) - n),
                    slide: updateRotation(1),
                    max: 360
                });
                rotation[1] = (radToDeg(rotation[1]) - n) * Math.PI / 180;
                break;
            case "f":
                this.console.log("angleY++");
                webglLessonsUI.setupSlider("#angleY", {
                    value: (radToDeg(rotation[1]) + n),
                    slide: updateRotation(1),
                    max: 360
                });
                rotation[1] = (radToDeg(rotation[1]) + n) * Math.PI / 180;
                break;
            case "a":
                this.console.log("z--");
                webglLessonsUI.setupSlider("#z", {
                    value: translation[2] - n,
                    slide: updatePosition(2),
                    min: -1000,
                    max: 0
                });
                translation[2] = translation[2] - n;
                break;
            case "z":
                this.console.log("z++");
                webglLessonsUI.setupSlider("#z", {
                    value: translation[2] + n,
                    slide: updatePosition(2),
                    min: -1000,
                    max: 0
                });
                translation[2] = translation[2] + n;
                break;
            case "e":
                this.console.log("angleZ--");
                webglLessonsUI.setupSlider("#angleZ", {
                    value: (radToDeg(rotation[2]) - n),
                    slide: updateRotation(2),
                    max: 360
                });
                rotation[2] = (radToDeg(rotation[2]) - n) * Math.PI / 180;
                break;
            case "r":
                this.console.log("angleZ++");
                webglLessonsUI.setupSlider("#angleZ", {
                    value: (radToDeg(rotation[2]) + n),
                    slide: updateRotation(2),
                    max: 360
                });
                rotation[2] = (radToDeg(rotation[2]) + n) * Math.PI / 180;
                break;
            case "ArrowDown":
                console.log("ArrowDown");
                break;
            case "ArrowUp":
                this.console.log("ArrowUp");
                break;
            case "Enter":
                this.console.log("Enter");
                break;
            case " ":
                this.console.log("Space");
                break;
            default:
                return; // Quitter lorsque cela ne gère pas l'événement touche.
        }

        drawScene();

        // Annuler l'action par défaut pour éviter qu'elle ne soit traitée deux fois.
        event.preventDefault();
    }, true);

}

main();