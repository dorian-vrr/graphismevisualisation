window.onload=function() {
    var inp = document.getElementById('file');
    inp.onchange = function() {
      readFile(this,function(res) { launchParser(res); });
    };
};
function readFile(input,callback) {
    if(typeof FileReader !== 'undefined') {
        var fr = new FileReader();
        fr.readAsText(input.files[0]);
        fr.onload = function() {
        callback(fr.result);
        };
    } else if(typeof ActiveXObject !== 'undefined') {
        var path = input.value,
        ts = (new ActiveXObject("Scripting.FileSystemObject")).GetFile(path).OpenAsTextStream(1,-2),
        res = '';
        while (!ts.AtEndOfStream) {
        res += ts.ReadLine() + '\n';
        }
        ts.Close();
        callback(res);
    }
};

let body = document.getElementById("body");
let choose =  document.getElementById("choose");
let canvas = document.getElementById("canvas");

let json = {};
function launchParser(file){
    console.log("Parser launched...");
    choose.style.display="none";
    body.style.backgroundColor="black";
    body.style.backgroundImage='url("images/loading.gif")';

    let lines = file.split('\n');
    lines.forEach(line => {
        linesplit = line.split(",");
        let annee=null;
        let allFos=[];
        if(linesplit[3]!=undefined)annee = linesplit[3];
        if(annee!="year" || annee!=undefined){
            if(linesplit[5]!=undefined)allFos = linesplit[5].split(";");
            if(json[annee]==undefined){
                json[annee] = [];
            }
            allFos.forEach(fos => {
                if(!(fos in json[annee])){
                    json[annee].push(fos);
                }
            })
        }
    });
    setTimeout(function(){
        console.log(json);
        body.style.backgroundImage='url()';
        canvas.style.display="block";
        canvas.style.height="100%";
        canvas.style.width="100%";
        // main();

        let arrays = [];



        // arrays.push({
        //     type:"soleil",
        //     ZoomLevel: -3,
        //     Rotation: {
        //         X: 0,
        //         Y: 0,
        //         Z: 0
        //     },
        //     Position: {
        //         X: -750,
        //         Y: 0
        //     }
        // });
        arrays.push({
            type:"planète",
            ZoomLevel: -4,
            Rotation: {
                X: 0.5,
                Y: 0.5,
                Z: 0
            },
            Position: {
                X: -500,
                Y: 0
            }
        });

        let x = -300;
        for(let i=0; i<5; i++){
            arrays.push({
                type:"planète",
                ZoomLevel: -16,
                Rotation: {
                    X: 0,
                    Y: 0.00025,
                    Z: 0
                },
                Position: {
                    X: x,
                    Y: 0
                }
            });
            x = x+150;
        }

        drawTest(arrays)
    }.bind(this),1000);
}
