const y_cont = "year_container";
const y_canv = "year_canvas";
const y_text = "year_text";

let planetesYear;
let planetesToDrawYear;

let translation;
let rotation;

let textCtx = document.createElement("canvas").getContext("2d");
let textProgramInfo;
let textBufferInfo;
let textPositions = [];
let textTextures;
let textUniforms;

year_display = () => {
    document.getElementById(y_canv).style.backgroundImage = 'url("resources/images/stars.jpg")';
    document.getElementById(y_cont).style.display = "block";
    document.getElementById(body).style.backgroundImage = 'url()';
}

year_draw = (data, year) => {
    console.log('year_draw : ', year, " | data : ", data);

    planetesYear = create_planetes_from_year(data, year);

    let canvas = document.getElementById(y_canv);
    gl = canvas.getContext("webgl");
    if (!gl) {
        return;
    }

    program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
    fProgramInfo = webglUtils.createProgramInfo(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
    textProgramInfo = webglUtils.createProgramInfo(gl, ["text-vertex-shader", "text-fragment-shader"]);
    matrixLocation = gl.getUniformLocation(program, "u_matrix");
    positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

    const texcoordLocation = gl.getAttribLocation(program, "a_texcoords");
    let buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.enableVertexAttribArray(texcoordLocation);
    gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0);

    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array(
            [
                -3, -1,
                2, -1,
                -3, 4,
                -3, 4,
                2, -1,
                2, 4,
            ]
        ),
        gl.STATIC_DRAW
    );

    planetesToDrawYear = create_planetes_to_draw_year();

    // START TEST

    textBufferInfo = primitives.createPlaneBufferInfo(gl, 1, 1, 1, 1, m4.xRotation(Math.PI / 2));


      textUniforms = {
        u_matrix: m4.identity(),
        u_texture: null,
      };

    // END TEST


    window.addEventListener("keydown", year_keydown, true);
    requestAnimationFrame(drawSceneYear);

    translation = [0, 0, -500]; //-500
    rotation = [degToRad(15), degToRad(0), degToRad(0)];
    initializeSliders(translation, rotation);
}

create_planetes_from_year = (data, year) => {
    let planetes_year = [];
    planetes_year.push({
            radius: 40,
            texture_image: "resources/textures/sun.jpg",
            translation: [0, 0, 0],
            position: -110,
            sun: true,
            text: year+" " + (data ? data.all_fos[0].fos : "NO DATA")
    })

    let textures = ["resources/textures/jupiter.jpg","resources/textures/make.jpg","resources/textures/earth.jpg","resources/textures/saturn.jpg","resources/textures/mars.jpg","resources/textures/moon.jpg","resources/textures/neptune.jpg","resources/textures/venus.jpg"]
    let positions = [
        [200, 0, 0],
        [70, 0, -200],
        [70, 0, 200],
        [120, 0, -100],
        [-70, 0, 140],
        [70, 0, -140],
        [200, 0, 100],
        [0, 0, 100],
    ]
    if(data){
        data.all_fos.forEach((fos, i) => {
            if(i!=0 && i<9){
                planetes_year.push({
                    radius: fos.cpt>100 ? fos.cpt/100 : 10,
                    texture_image: textures[i-1],
                    translation: positions[i-1],
                    sun: false,
                    text: fos.fos
                });
            }
        })
    }

    return planetes_year;
}

create_planetes_to_draw_year = () => {
    return planetesYear.map(planete => {
        const sphere = createFlattenedVertices(gl, primitives.createSphereVertices(planete.radius, subdivisionsAxis, subdivisionsHeight));
        const texture = createTexture(planete.texture_image);
        const sphere_uniform = { u_texture: texture, u_matrix: m4.identity() };

        return {
            programInfo: fProgramInfo,
            bufferInfo: sphere,
            uniforms: sphere_uniform,
            translation: planete.translation,
            sun: planete.sun,
            text: planete.text,
            radius : planete.radius
        };
    });
}

drawSceneYear = (clock) => {
    clock *= 0.001;
    let deltaTime = clock - then;
    then = clock;

    const scale = [1, 1, 1];
    const fieldOfViewRadians = degToRad(60);

    webglUtils.resizeCanvasToDisplaySize(gl.canvas);

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.disable(gl.BLEND);
    gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);
    gl.depthMask(true);
    gl.useProgram(program);

    var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    var zNear = 1;
    var zFar = 2000;
    var matrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
    matrix = m4.translate(matrix, translation[0], translation[1], translation[2]);
    matrix = m4.xRotate(matrix, rotation[0]);
    matrix = m4.yRotate(matrix, rotation[1]);
    matrix = m4.zRotate(matrix, rotation[2]);
    matrix = m4.scale(matrix, scale[0], scale[1], scale[2]);

    gl.uniformMatrix4fv(matrixLocation, false, matrix);

    // const rotationY = clock;
    const rotationY = 0;

    planetesToDrawYear.forEach(planete => {
        planete.uniforms.u_matrix = computeMatrix(
            matrix,
            planete.translation,
            0,
            rotationY
        );
        
         // drawText
        textPositions.push({text: planete.text, matrix: planete.uniforms.u_matrix, radius: planete.radius});

        gl.useProgram(planete.programInfo.program);
        webglUtils.setBuffersAndAttributes(gl, planete.programInfo, planete.bufferInfo);
        webglUtils.setUniforms(planete.programInfo, planete.uniforms);
       
        gl.drawArrays(gl.TRIANGLES, 0, planete.bufferInfo.numElements);


    });

    display_text_year();
    drawText();
    requestAnimationFrame(drawSceneYear);
}

year_keydown = (event) => {
    const n = 5;

    if (event.defaultPrevented) {
        return;
    }

    switch (event.key) {
        case "w":
            webglLessonsUI.setupSlider("#x", {
                value: translation[0] - n,
                slide: updatePosition(0),
                min: -200,
                max: 200
            });
            translation[0] = translation[0] - n;
            break;
        case "x":
            webglLessonsUI.setupSlider("#x", {
                value: translation[0] + n,
                slide: updatePosition(0),
                min: -200,
                max: 200
            });
            translation[0] = translation[0] + n;
            break;
        case "c":
            webglLessonsUI.setupSlider("#angleX", {
                value: (radToDeg(rotation[0]) - n),
                slide: updateRotation(0),
                max: 360
            });
            rotation[0] = (radToDeg(rotation[0]) - n) * Math.PI / 180;
            break;
        case "v":
            webglLessonsUI.setupSlider("#angleX", {
                value: (radToDeg(rotation[0]) + n),
                slide: updateRotation(0),
                max: 360
            });
            rotation[0] = (radToDeg(rotation[0]) + n) * Math.PI / 180;
            break;
        case "q":
            webglLessonsUI.setupSlider("#y", {
                value: translation[1] - n,
                slide: updatePosition(1),
                min: -200,
                max: 200
            });
            translation[1] = translation[1] - n;
            break;
        case "s":
            webglLessonsUI.setupSlider("#y", {
                value: translation[1] + n,
                slide: updatePosition(1),
                min: -200,
                max: 200
            });
            translation[1] = translation[1] + n;
            break;
        case "d":
            webglLessonsUI.setupSlider("#angleY", {
                value: (radToDeg(rotation[1]) - n),
                slide: updateRotation(1),
                max: 360
            });
            rotation[1] = (radToDeg(rotation[1]) - n) * Math.PI / 180;
            break;
        case "f":
            webglLessonsUI.setupSlider("#angleY", {
                value: (radToDeg(rotation[1]) + n),
                slide: updateRotation(1),
                max: 360
            });
            rotation[1] = (radToDeg(rotation[1]) + n) * Math.PI / 180;
            break;
        case "a":
            webglLessonsUI.setupSlider("#z", {
                value: translation[2] - n,
                slide: updatePosition(2),
                min: -1000,
                max: 0
            });
            translation[2] = translation[2] - n;
            break;
        case "z":
            webglLessonsUI.setupSlider("#z", {
                value: translation[2] + n,
                slide: updatePosition(2),
                min: -1000,
                max: 0
            });
            translation[2] = translation[2] + n;
            break;
        case "e":
            webglLessonsUI.setupSlider("#angleZ", {
                value: (radToDeg(rotation[2]) - n),
                slide: updateRotation(2),
                max: 360
            });
            rotation[2] = (radToDeg(rotation[2]) - n) * Math.PI / 180;
            break;
        case "r":
            webglLessonsUI.setupSlider("#angleZ", {
                value: (radToDeg(rotation[2]) + n),
                slide: updateRotation(2),
                max: 360
            });
            rotation[2] = (radToDeg(rotation[2]) + n) * Math.PI / 180;
            break;
        case "ArrowUp":
            webglLessonsUI.setupSlider("#z", {
                value: translation[2] + n,
                slide: updatePosition(2),
                min: -1000,
                max: 0
            });
            translation[2] = translation[2] + n;
            break;
        case "ArrowDown":
            webglLessonsUI.setupSlider("#z", {
                value: translation[2] - n,
                slide: updatePosition(2),
                min: -1000,
                max: 0
            });
            translation[2] = translation[2] - n;
            break;
        case "ArrowLeft":
            webglLessonsUI.setupSlider("#x", {
                value: translation[0] + n,
                slide: updatePosition(0),
                min: -1000,
                max: 0
            });
            translation[0] = translation[0] + n;
            break;
        case "ArrowRight":
            webglLessonsUI.setupSlider("#x", {
                value: translation[0] - n,
                slide: updatePosition(0),
                min: -1000,
                max: 0
            });
            translation[0] = translation[0] - n;
            break;
        case "Escape":
            back();
            break;
        default:
            return;
    }

    event.preventDefault();
}

initializeSliders = (translation, rotation) => {
    webglLessonsUI.setupSlider("#x", {
        value: translation[0],
        slide: updatePosition(0),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#y", {
        value: translation[1],
        slide: updatePosition(1),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#z", {
        value: translation[2],
        slide: updatePosition(2),
        min: -1000,
        max: 0
    });
    webglLessonsUI.setupSlider("#angleX", {
        value: radToDeg(rotation[0]),
        slide: updateRotation(0),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleY", {
        value: radToDeg(rotation[1]),
        slide: updateRotation(1),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleZ", {
        value: radToDeg(rotation[2]),
        slide: updateRotation(2),
        max: 360
    });
}

updatePosition = (index) => {
    return (event, ui) => {
        translation[index] = ui.value;
        drawSceneYear();
    };
}

updateRotation = (index) => {
    return (event, ui) => {
        var angleInDegrees = ui.value;
        var angleInRadians = angleInDegrees * Math.PI / 180;
        rotation[index] = angleInRadians;
        drawSceneYear();
    };
}

display_text_year = () => {
    resetDivSets(y_text);
    addDivSet(y_text, "FOS Solar Système", 650, 100);
}

drawText = () => {
    let color = [1, 1, 1, 1];//[0.0, 1.0, 0.0, 1];
    

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
    gl.depthMask(false);

    // setup to draw the text.
    gl.useProgram(textProgramInfo.program);

    webglUtils.setBuffersAndAttributes(gl, textProgramInfo, textBufferInfo);

    textPositions.forEach(textPosition => {
        let text_list = []; text_list.push(textPosition.text);
        text_list=text_list.map(text => {
            const textCanvas = makeTextCanvas(text, 200, 50);
            const textWidth  = textCanvas.width;
            const textHeight = textCanvas.height;
            const textTex = gl.createTexture();
            gl.bindTexture(gl.TEXTURE_2D, textTex);
            gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, textCanvas);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            return {
                texture: textTex,
                width: textWidth,
                height: textHeight,
            };
        });
        let text = text_list[0];

        var textMatrix = m4.translate(textPosition.matrix, 0, 0, textPosition.radius);
        textMatrix = m4.scale(textMatrix, (text.width), (text.height), 1);

        m4.copy(textMatrix, textUniforms.u_matrix);

        textUniforms.u_texture = text.texture;
        textUniforms.u_color = color;
        webglUtils.setUniforms(textProgramInfo, textUniforms);

        // Draw the text.
        gl.drawElements(gl.TRIANGLES, textBufferInfo.numElements, gl.UNSIGNED_SHORT, 0);

        textPositions = [];
        gl.depthMask(true);
    });

}


// Puts text in center of canvas.
makeTextCanvas = (text, width, height) => {
    textCtx.canvas.width  = width;
    textCtx.canvas.height = height;
    textCtx.font = "18px sans-serif";
    textCtx.textAlign = "center";
    textCtx.textBaseline = "middle";
    textCtx.fillStyle = "white";
    textCtx.clearRect(0, 0, textCtx.canvas.width, textCtx.canvas.height);
    textCtx.fillText(text, width / 2, height / 2);
    return textCtx.canvas;
  }
  
