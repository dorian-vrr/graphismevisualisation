const m_cont = "menu_container";
const m_canv = "menu_canvas";
const m_text = "menu_text";

let planetesMenu;
let planetesToDrawMenu;

menu_display = () => {
  document.getElementById(m_canv).style.backgroundImage = 'url("resources/images/stars.jpg")';
  document.getElementById(m_cont).style.display = "block";
  document.getElementById(body).style.backgroundImage = 'url()';
}

menu_draw = (json_entry, year) => {
  console.log("menu_draw : ", json_entry);
  planetesMenu = create_planetes_from_json(json_entry, year);

  let canvas = document.getElementById(m_canv);
  gl = canvas.getContext("webgl");
  if (!gl) {
    return;
  }

  program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
  fProgramInfo = webglUtils.createProgramInfo(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
  positionLocation = gl.getAttribLocation(program, "a_position");
  colorLocation = gl.getAttribLocation(program, "a_color");
  positionBuffer = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  colorBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

  const texcoordLocation = gl.getAttribLocation(program, "a_texcoords");
  let buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.enableVertexAttribArray(texcoordLocation);
  gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0);

  gl.bufferData(
    gl.ARRAY_BUFFER,
    new Float32Array(
      [
        -3, -1,
        2, -1,
        -3, 4,
        -3, 4,
        2, -1,
        2, 4,
      ]
    ),
    gl.STATIC_DRAW
  );
  
  planetesToDrawMenu = create_planetes_to_draw_menu();

  window.addEventListener("keydown", menu_keydown, true);
  requestAnimationFrame(drawSceneMenu);
}

create_planetes_from_json = (json, year) => {
  return [
    {
      radius: 20,
      texture_image: "resources/textures/sun.jpg",
      translation: [-120, 0, 0],
      position: -110,
      sun: true
    },
    {
      radius: 10,
      texture_image: "resources/textures/jupiter.jpg",
      translation: [-40, 0, 0],
      position: -44,
      sun: false,
      year: year-2
    },
    {
      radius: 10,
      texture_image: "resources/textures/make.jpg",
      translation: [-5, 0, 0],
      position: -9,
      sun: false,
      year: year-1
    },
    {
      radius: 16,
      texture_image: "resources/textures/earth.jpg",
      translation: [30, 0, 0],
      position: 28,
      sun: false,
      year: year
    },
    {
      radius: 10,
      texture_image: "resources/textures/mars.jpg",
      translation: [65, 0, 0],
      position: 63,
      sun: false,
      year: year+1
    },
    {
      radius: 10,
      texture_image: "resources/textures/saturn.jpg",
      translation: [100, 0, 0],
      position: 98,
      sun: false,
      year: year+2
    }
  ];
}

create_planetes_to_draw_menu = () => {
  return planetesMenu.map(planete => {
    const sphere = createFlattenedVertices(gl, primitives.createSphereVertices(planete.radius, subdivisionsAxis, subdivisionsHeight));
    const texture = createTexture(planete.texture_image);
    const sphere_uniform = { u_texture: texture, u_matrix: m4.identity() };

    return {
      programInfo: fProgramInfo,
      bufferInfo: sphere,
      uniforms: sphere_uniform,
      pos: planete.position,
      translation: planete.translation,
      sun: planete.sun,
      year:  planete.year || null
    };
  });
}

drawSceneMenu = (clock) => {
  clock *= 0.001;
  let deltaTime = clock - then;
  then = clock;

  const rotation = [degToRad(190), degToRad(0), degToRad(0)];
  const fieldOfViewRadians = degToRad(60);
  const rotationSpeed = 1.2;
  divSetNdx = 0;
  divSets = [];

  rotation[1] += rotationSpeed * deltaTime;
  rotation[2] += deltaTime;

  webglUtils.resizeCanvasToDisplaySize(gl.canvas);

  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.enable(gl.CULL_FACE);
  gl.enable(gl.DEPTH_TEST);
  gl.useProgram(program);
  gl.enableVertexAttribArray(positionLocation);
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  let size = 3;          // 3 components per iteration
  let type = gl.FLOAT;   // the data is 32bit floats
  let normalize = false; // don't normalize the data
  let stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  let offset = 0;        // start at the beginning of the buffer

  gl.vertexAttribPointer(positionLocation, size, type, normalize, stride, offset);
  gl.enableVertexAttribArray(colorLocation);
  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

  size = 3;                 // 3 components per iteration
  type = gl.UNSIGNED_BYTE;  // the data is 8bit unsigned values
  normalize = true;         // normalize the data (convert from 0-255 to 0-1)
  stride = 0;               // 0 = move forward size * sizeof(type) each iteration to get the next position
  offset = 0;               // start at the beginning of the buffer

  gl.vertexAttribPointer(colorLocation, size, type, normalize, stride, offset);

  let aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
  const zNear = 1;
  const zFar = 2000;
  let projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

  aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
  projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, 1, 2000);

  const cameraPosition = [0, 0, 100];
  const target = [0, 0, 0];
  const up = [0, 1, 0];
  const cameraMatrix = m4.lookAt(cameraPosition, target, up);

  const viewMatrix = m4.inverse(cameraMatrix);
  const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

  let rotationX = clock;
  let rotationY = clock;

  planetesToDrawMenu.forEach(planete => {
    planete.uniforms.u_matrix = computeMatrix(
      viewProjectionMatrix,
      planete.translation,
      rotationX,
      rotationY);
    
    if(!planete.sun)planete.clipspace = m4.transformVector(viewProjectionMatrix, [planete.pos, -20, 0, 1]);
  
    gl.useProgram(planete.programInfo.program);
    webglUtils.setBuffersAndAttributes(gl, planete.programInfo, planete.bufferInfo);
    webglUtils.setUniforms(planete.programInfo, planete.uniforms);
    gl.drawArrays(gl.TRIANGLES, 0, planete.bufferInfo.numElements);
  });

  display_text();
  requestAnimationFrame(drawSceneMenu);
}

menu_keydown = (event) => {
  if (event.defaultPrevented) {
    return;
  }

  switch (event.key) {
    case "ArrowLeft":
      planetesToDrawMenu.forEach(planete => {
        planete.year--;
      });
      display_text();
      break;
    case "ArrowRight":
      planetesToDrawMenu.forEach(planete => {
        planete.year++;
      });
      display_text();
      break;
    case "Enter":
      year_chosen(planetesToDrawMenu[3].year);
      break;
    default:
      return;
  }

  event.preventDefault();
}

display_text = () => {
  resetDivSets(m_text);
  addDivSet(m_text,"FOS Solar Système", 650, 100);
  planetesToDrawMenu.forEach(planete => {
    if (planete.clipspace) {
      const clipspace = planete.clipspace;
      clipspace[0] /= clipspace[3];
      clipspace[1] /= clipspace[3];
      const pixelX = (clipspace[0] * 0.5 + 0.5) * gl.canvas.width;
      const pixelY = (clipspace[1] * -0.5 + 0.5) * gl.canvas.height;
      addDivSet(m_text, planete.year, pixelX, pixelY);
    }
  });
}