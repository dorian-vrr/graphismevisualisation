const body = "body";
const menu = "menu_container";
const year = "year_container";

let alldata;
let last_year;

window.onload = () => {
    document.getElementById('file').onchange = function() {
        loading();
        readFile(this, (res) => {
            launchParser(res, (years) => {
                menu_display();
                all_years = years;
                menu_draw(all_years, 2000);
            });
        });
    };
};

loading = () => {
    document.getElementById(body).style.backgroundImage='url("resources/images/loading.gif")';
    document.getElementById("choose").style.display="none";
}
lightspeed = () =>{
    document.getElementById(body).style.backgroundImage='url("resources/images/lightspeed.gif")';
    document.getElementById(body).style.backgroundSize = "cover";
    document.getElementById(menu).style.display="none";
    document.getElementById(year).style.display="none";
}

wait = (fct, time) => {
    setTimeout(() => { fct(); },time*1000);
}

year_chosen = (year) => {
    let data;
    last_year = year;
    all_years.forEach(one_year => {
        if(one_year.year == year){
            data = one_year;
        }
    });
    lightspeed();
    window.removeEventListener("keydown", menu_keydown, true);
    wait(() => {year_display();year_draw(data, last_year)}, 2);
}

back = () =>{
    lightspeed();
    window.removeEventListener("keydown", year_keydown, true);
    wait(() => {menu_display();menu_draw(all_years, last_year)}, 2);
}