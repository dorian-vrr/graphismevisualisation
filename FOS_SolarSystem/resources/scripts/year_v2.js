const y_cont = "year_container";
const y_canv = "year_canvas";
const y_text = "year_text";

let planetesYear;
let planetesToDrawYear;

let translation;
let rotation;

year_display = () => {
    document.getElementById(y_canv).style.backgroundImage = 'url("resources/images/stars.jpg")';
    document.getElementById(y_cont).style.display = "block";
    document.getElementById(body).style.backgroundImage = 'url()';
}

year_draw = (year) => {
    console.log('year_draw : ' + year);

    planetesYear = create_planetes_from_year(year);

    let canvas = document.getElementById(y_canv);
    gl = canvas.getContext("webgl");
    if (!gl) {
        return;
    }

    program = webglUtils.createProgramFromScripts(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
    fProgramInfo = webglUtils.createProgramInfo(gl, ["3d-vertex-shader", "3d-fragment-shader"]);
    matrixLocation = gl.getUniformLocation(program, "u_matrix");
    positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

    const texcoordLocation = gl.getAttribLocation(program, "a_texcoords");
    let buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.enableVertexAttribArray(texcoordLocation);
    gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0);

    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array(
            [
                -3, -1,
                2, -1,
                -3, 4,
                -3, 4,
                2, -1,
                2, 4,
            ]
        ),
        gl.STATIC_DRAW
    );

    planetesToDrawYear = create_planetes_to_draw_year();

    window.addEventListener("keydown", year_keydown, true);
    requestAnimationFrame(drawSceneYear);

    translation = [0, 0, -500];
    rotation = [degToRad(190), degToRad(0), degToRad(0)];
    initializeSliders(translation, rotation);
}

create_planetes_from_year = (year) => {
    return [
        {
            radius: 40,
            subdivisionsAxis: 44,
            subdivisionsHeight: 12,
            texture_image: "resources/textures/sun.jpg",
            translation: [0, 0, 0],
            position: -110,
            sun: true
        },
        {
            radius: 27,
            subdivisionsAxis: 30,
            subdivisionsHeight: 10,
            texture_image: "resources/textures/jupiter.jpg",
            translation: [-100, 0, -10],
            position: -44,
            sun: false,
            year: 1998
        },
        {
            radius: 20,
            subdivisionsAxis: 25,
            subdivisionsHeight: 7,
            texture_image: "resources/textures/make.jpg",
            translation: [100, 0, 0],
            position: -9,
            sun: false,
            year: 1999
        },
        {
            radius: 10,
            subdivisionsAxis: 12,
            subdivisionsHeight: 6,
            texture_image: "resources/textures/earth.jpg",
            translation: [70, 0, -200],
            position: 28,
            sun: false,
            year: 2000
        },
        {
            radius: 10,
            subdivisionsAxis: 12,
            subdivisionsHeight: 6,
            texture_image: "resources/textures/mars.jpg",
            translation: [70, 0, 200],
            position: 63,
            sun: false,
            year: 2001
        },
        {
            radius: 10,
            subdivisionsAxis: 12,
            subdivisionsHeight: 60,
            texture_image: "resources/textures/saturn.jpg",
            translation: [100, 0, -100],
            position: 98,
            sun: false,
            year: 2002
        }
    ];
}

create_planetes_to_draw_year = () => {
    return planetesYear.map(planete => {
        const sphere = createFlattenedVertices(gl, primitives.createSphereVertices(planete.radius, planete.subdivisionsAxis, planete.subdivisionsHeight));
        const texture = createTexture(planete.texture_image);
        const sphere_uniform = { u_texture: texture, u_matrix: m4.identity() };

        return {
            programInfo: fProgramInfo,
            bufferInfo: sphere,
            uniforms: sphere_uniform,
            pos: planete.position,
            translation: planete.translation,
            sun: planete.sun,
            year: planete.year || null
        };
    });
}

drawSceneYear = (clock) => {
    clock *= 0.001;
    let deltaTime = clock - then;
    then = clock;

    const scale = [1, 1, 1];
    const fieldOfViewRadians = degToRad(60);
    divSetNdx = 0;
    divSets = [];

    webglUtils.resizeCanvasToDisplaySize(gl.canvas);

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);
    gl.useProgram(program);

    var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    var zNear = 1;
    var zFar = 2000;
    var matrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
    matrix = m4.translate(matrix, translation[0], translation[1], translation[2]);
    matrix = m4.xRotate(matrix, rotation[0]);
    matrix = m4.yRotate(matrix, rotation[1]);
    matrix = m4.zRotate(matrix, rotation[2]);
    matrix = m4.scale(matrix, scale[0], scale[1], scale[2]);

    gl.uniformMatrix4fv(matrixLocation, false, matrix);

    planetesToDrawYear.forEach(planete => {
        planete.uniforms.u_matrix = computeMatrix(
            matrix,
            planete.translation,
            0,
            0
        );

        // if (!planete.sun) planete.clipspace = m4.transformVector(viewProjectionMatrix, [planete.pos, -20, 0, 1]);

        gl.useProgram(planete.programInfo.program);
        webglUtils.setBuffersAndAttributes(gl, planete.programInfo, planete.bufferInfo);
        webglUtils.setUniforms(planete.programInfo, planete.uniforms);
        gl.drawArrays(gl.TRIANGLES, 0, planete.bufferInfo.numElements);
    });

    display_text_year();
    requestAnimationFrame(drawSceneYear);
}

year_keydown = (event) => {
    const n = 5;

    if (event.defaultPrevented) {
        return;
    }

    switch (event.key) {
        case "w":
            this.console.log("x--");
            webglLessonsUI.setupSlider("#x", {
                value: translation[0] - n,
                slide: updatePosition(0),
                min: -200,
                max: 200
            });
            translation[0] = translation[0] - n;
            break;
        case "x":
            this.console.log("x++");
            webglLessonsUI.setupSlider("#x", {
                value: translation[0] + n,
                slide: updatePosition(0),
                min: -200,
                max: 200
            });
            translation[0] = translation[0] + n;
            break;
        case "c":
            this.console.log("angleX--");
            webglLessonsUI.setupSlider("#angleX", {
                value: (radToDeg(rotation[0]) - n),
                slide: updateRotation(0),
                max: 360
            });
            rotation[0] = (radToDeg(rotation[0]) - n) * Math.PI / 180;
            break;
        case "v":
            this.console.log("angleX++");
            webglLessonsUI.setupSlider("#angleX", {
                value: (radToDeg(rotation[0]) + n),
                slide: updateRotation(0),
                max: 360
            });
            rotation[0] = (radToDeg(rotation[0]) + n) * Math.PI / 180;
            break;
        case "q":
            this.console.log("y--");
            webglLessonsUI.setupSlider("#y", {
                value: translation[1] - n,
                slide: updatePosition(1),
                min: -200,
                max: 200
            });
            translation[1] = translation[1] - n;
            break;
        case "s":
            this.console.log("y++");
            webglLessonsUI.setupSlider("#y", {
                value: translation[1] + n,
                slide: updatePosition(1),
                min: -200,
                max: 200
            });
            translation[1] = translation[1] + n;
            break;
        case "d":
            this.console.log("angleY--");
            webglLessonsUI.setupSlider("#angleY", {
                value: (radToDeg(rotation[1]) - n),
                slide: updateRotation(1),
                max: 360
            });
            rotation[1] = (radToDeg(rotation[1]) - n) * Math.PI / 180;
            break;
        case "f":
            this.console.log("angleY++");
            webglLessonsUI.setupSlider("#angleY", {
                value: (radToDeg(rotation[1]) + n),
                slide: updateRotation(1),
                max: 360
            });
            rotation[1] = (radToDeg(rotation[1]) + n) * Math.PI / 180;
            break;
        case "a":
            this.console.log("z--");
            webglLessonsUI.setupSlider("#z", {
                value: translation[2] - n,
                slide: updatePosition(2),
                min: -1000,
                max: 0
            });
            translation[2] = translation[2] - n;
            break;
        case "z":
            this.console.log("z++");
            webglLessonsUI.setupSlider("#z", {
                value: translation[2] + n,
                slide: updatePosition(2),
                min: -1000,
                max: 0
            });
            translation[2] = translation[2] + n;
            break;
        case "e":
            this.console.log("angleZ--");
            webglLessonsUI.setupSlider("#angleZ", {
                value: (radToDeg(rotation[2]) - n),
                slide: updateRotation(2),
                max: 360
            });
            rotation[2] = (radToDeg(rotation[2]) - n) * Math.PI / 180;
            break;
        case "r":
            this.console.log("angleZ++");
            webglLessonsUI.setupSlider("#angleZ", {
                value: (radToDeg(rotation[2]) + n),
                slide: updateRotation(2),
                max: 360
            });
            rotation[2] = (radToDeg(rotation[2]) + n) * Math.PI / 180;
            break;
        case "Escape":
            back();
            break;
        default:
            return;
    }

    event.preventDefault();
}

initializeSliders = (translation, rotation) => {
    webglLessonsUI.setupSlider("#x", {
        value: translation[0],
        slide: updatePosition(0),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#y", {
        value: translation[1],
        slide: updatePosition(1),
        min: -200,
        max: 200
    });
    webglLessonsUI.setupSlider("#z", {
        value: translation[2],
        slide: updatePosition(2),
        min: -1000,
        max: 0
    });
    webglLessonsUI.setupSlider("#angleX", {
        value: radToDeg(rotation[0]),
        slide: updateRotation(0),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleY", {
        value: radToDeg(rotation[1]),
        slide: updateRotation(1),
        max: 360
    });
    webglLessonsUI.setupSlider("#angleZ", {
        value: radToDeg(rotation[2]),
        slide: updateRotation(2),
        max: 360
    });
}

updatePosition = (index) => {
    return (event, ui) => {
        translation[index] = ui.value;
        drawSceneYear();
    };
}

updateRotation = (index) => {
    return (event, ui) => {
        var angleInDegrees = ui.value;
        var angleInRadians = angleInDegrees * Math.PI / 180;
        rotation[index] = angleInRadians;
        drawSceneYear();
    };
}

display_text_year = () => {
    resetDivSets(y_text);
    addDivSet(y_text, "FOS Solar Système", 650, 100);
    planetesToDrawYear.forEach(planete => {
        //     if (planete.clipspace) {
        //       const clipspace = planete.clipspace;
        //       clipspace[0] /= clipspace[3];
        //       clipspace[1] /= clipspace[3];
        //       const pixelX = (clipspace[0] * 0.5 + 0.5) * gl.canvas.width;
        //       const pixelY = (clipspace[1] * -0.5 + 0.5) * gl.canvas.height;
        //       addDivSet(planete.year, pixelX, pixelY);
        //     }
    });
}

