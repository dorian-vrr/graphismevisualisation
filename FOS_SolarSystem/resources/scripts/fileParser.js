readFile = (input, callback) => {
    if (typeof FileReader !== 'undefined') {
        var fr = new FileReader();
        fr.readAsText(input.files[0]);
        fr.onload = function () {
            return callback(fr.result);
        };
    } else if (typeof ActiveXObject !== 'undefined') {
        var path = input.value,
            ts = (new ActiveXObject("Scripting.FileSystemObject")).GetFile(path).OpenAsTextStream(1, -2),
            res = '';
        while (!ts.AtEndOfStream) {
            res += ts.ReadLine() + '\n';
        }
        ts.Close();
        return callback(res);
    }
};

launchParser = (file, callback) => {
    console.log("Parser launched...");
    let years = [];
    let lines = file.split('\n');
    lines.forEach(line => {
        linesplit = line.split(",");
        let annee;
        let allFos;
        linesplit[3] ? annee = parseInt(linesplit[3].substring(1, 5)) : annee = NaN;
        if (!isNaN(annee)) {
            linesplit[5] ? allFos = linesplit[5].substring(1, linesplit[5].length - 1).split(";") : allFos = [];
            
            let exist = false;
            years.forEach(fos_year => {
                if (fos_year.year == annee){
                    exist = true;

                    allFos.forEach(ffos => {
                        fos_year.all_fos.forEach(ffos2 => {
                            if(ffos.split(":")[0]==ffos2.fos){
                                ffos2.cpt++;
                            }
                        });
                    })
                }
            });
            if(!exist){
                years.push({
                    year: annee,
                    all_fos: allFos.map(fos => {
                        return {fos: fos.split(":")[0], cpt: 1};
                    })
                });
            }
        }
    });

    years = years.map(year => {
        return {year: year.year, all_fos: year.all_fos.sort(triFos)};
    })
    callback(years.sort(triYear));
}

alreadyExist = (annee, years) => {
    let exist = false;
    years.forEach(fos_year => { if (fos_year.year == annee) exist = true; });
    return exist;
}

triYear = (a, b) => {
    if (a.year < b.year)
        return -1;
    else if (a.year > b.year)
        return 1;
    return 0;
}

triFos = (a, b) => {
    if (a.cpt < b.cpt)
        return 1;
    else if (a.cpt > b.cpt)
        return -1;
    return 0;
}